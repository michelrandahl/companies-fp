Companies in Denmark using FP 
=============================

> **Note**: If you want to update the README file, please make a pull request
> and link to source(s), preferable done by tthe company itself. Also, please
> ensure that you keep the list sorted alphabetically.

* [Bilagscan](#bilagscan)
* [DBC](#dbc-as)
* [Delegate](#delegate-as)
* [Deon Digital](#deon-digital)
* [Dixa](#dixa)
* [GoMore](#go-more)
* [Hyperfactors](#hyperfactors)
* [Issuu](#issuu)
* [Motorola Solutions](#motorola-solutions)
* [Motosumu](#motosumu)
* [Paqle](#paqle-as)
* [Prolog Development Center](#prolog-development-center)
* [Relink](#relink)
* [Sabre CMS](#sabre-cms)
* [Shape](#shape)
* [Shopgun](#shopgun)
* [Simcorp](#simcorp)
* [UFST](#UFST)
* [Unity](#unity)

---

## Bilagscan

bilagscan - the future of programming (probabilistic programming)

## DBC A/S

Has a team working with [NixOS](https://nixos.org)

## Delegate A/S

<http://delegateas.github.io/> - F#

## Deon Digital

Deon Digital (Haskell and Kotlin, ñam!)

## Dixa

Dixa - (Scala Akka)

## GoMore

goMore (clojure)

## Hyperfactors

Hyperfactors (Scala an some probabilistic programming)

## Issuu

One team at Issuu uses OCaml almost exclusively (see [engineering
blog](https://engineering.issuu.com/) for details), other teams use
Erlang/Elixir and Python and Node.

## Motorola Solutions

Motorola Solutions (one team is using Elixir)

## Motosumu

motosumo (elixir)

## Paqle A/S

<www.paqle.com> - Scala

## Prolog Development Center

Prolog Development Center - PDC (visual prolog)

## Relink

Relink (Scala but also Node.js)

## Sabre CMS

Sabre CMS (Tons of Clojure and ClojureScript)

## Shape

Shape in some extent (probably very little, Erlang and Elixir)

## Shopgun

Shopgun - Erlang (This I will consider a functional programming oriented company)

## Simcorp

Simcorp - (a little bit of OCaml. They have a tiny team of 10 mostly OCaml
developers where they also have to know C## and some APL. Not functional
programming oriented where the most functional programming position is expecting
to master one of : C#, OCaml and APL and be proficient in two of them)

## UFST

Udviklings- og Forenklingsstyrelsen.
Project ICE (Implementerings Center for Ejendomsvurdering) uses Clojure for
both backend and frontend (Clojure-script).
Also use AWS and Event Sourcing techniques.
10-20 Clojure devlopers.
Last job-advert (deadline 16-01-2019): https://candidate.hr-manager.net/ApplicationInit.aspx?cid=5001&ProjectId=118833&DepartmentId=9414&MediaId=5
(Project ICE has previously been a part of Skatteministeriet)

## Unity

Unity uses F## in their developer tools team (CDS).
